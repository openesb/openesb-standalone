package net.openesb.standalone.startup;

import com.jenkov.cliargs.CliArgs;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Bootstrap {

    /**
     * JSR208 interfaces.
     */
    private static final String JBI_JAR_NAME = "jbi.jar";
    /**
     * JBI runtime interfaces exposed to components.
     */
    private static final String JBI_EXT_JAR_NAME = "jbi-ext.jar";
    /**
     * List of jars that should not be included in the runtime classloader.
     */
    private List<String> mBlacklistJars = new ArrayList<String>();
    private static String mUserSwitch = "-u"; 
    private static String mPasswordSwitch = "-p" ; 

    {
        mBlacklistJars.add(JBI_JAR_NAME);
        mBlacklistJars.add(JBI_EXT_JAR_NAME);
    }
    /**
     * ClassLoader used for JBI runtime classes. These classes are not part of
     * the component classloader hierarchy.
     */
    private ClassLoader mFrameworkClassLoader;
    /**
     * ClassLoader for clases in lib/ext that become part of the component
     * classloader hierarchy.
     */
    private ClassLoader mExtensionClassLoader;
    private final static Logger mLog =
            Logger.getLogger(Bootstrap.class.getName());
    /**
     * Daemon reference
     */
    private Object openesbDaemon = null;
    /**
     * Daemon object used by main.
     */
    private static Bootstrap daemon = null;
    private static final String OPENESB_HOME_PROP = "openesb.home";

    public void init(boolean stop) throws Exception {
        // Set OpenESB path
        setOpenesbHome();

        initClassLoaders();

        // Set the thread context classloader to the framework classloader
        Thread.currentThread().setContextClassLoader(mFrameworkClassLoader);

        Class<?> fwClass = mFrameworkClassLoader.loadClass(
                "net.openesb.standalone.startup.Container");
        Constructor defaultConstructor = fwClass.getConstructors()[0];
        Object startupInstance = defaultConstructor.newInstance(stop);

        openesbDaemon = startupInstance;
    }

    private void initClassLoaders() {
        createExtensionClassLoader();
        createFrameworkClassLoader();
    }

    /**
     * Set the
     * <code>openesb.home</code> System property to the current working
     * directory if it has not been set.
     */
    private void setOpenesbHome() {
        String installPath = System.getProperty(OPENESB_HOME_PROP);
        if (installPath == null) {
            File installDir = new File(System.getProperty("user.dir"));
            // account for javaw launch from a double-click on the jar
            if (installDir.getName().equals("lib")) {
                installDir = installDir.getParentFile();
            }

            installPath = installDir.getAbsolutePath();
        }

        File openesbHomeDir = new File(installPath);

        // quick sanity check on the install root
        if (!openesbHomeDir.isDirectory()
                || !new File(openesbHomeDir, "lib/jbi_rt.jar").exists()) {
            throw new RuntimeException("Invalid OpenESB Home: "
                    + openesbHomeDir.getAbsolutePath());
        }

        // pass this information along to the core framework
        System.setProperty(OPENESB_HOME_PROP,
                openesbHomeDir.getAbsolutePath());
    }

    /**
     * Start the OpenESB Standalone daemon.
     */
    public void start (String user, String password)  throws Exception {
        if (openesbDaemon == null) {
            init(false);
        }
        Class [] parameterClasses = {String.class, String.class}; 
        String [] parameters = {user, password} ;
        
        Method method = openesbDaemon.getClass().getMethod("start", parameterClasses);
        method.invoke(openesbDaemon, (Object[]) parameters);
    }
    
    /**
     * Stop the OpenESB Standalone daemon.
     */
    public void stop (String user, String password) throws Exception {
        if (openesbDaemon == null) {
            init(true);
        }
        Class [] parameterClasses = {String.class, String.class}; 
        String [] parameters = {user, password} ;
        
        Method method = openesbDaemon.getClass().getMethod("stop", parameterClasses);
        method.invoke(openesbDaemon, (Object[]) parameters);
    }

    /**
     * Return OpenESB status Started, Stopped, Undefined 
     * @param user
     * @param password
     * @throws java.lang.Exception
     */

    public void status (String user, String password) throws Exception {             
        if (openesbDaemon == null) {
            init(true);
        }
        Class [] parameterClasses = {String.class, String.class}; 
        String [] parameters = {user, password} ;
        
        Method method = openesbDaemon.getClass().getMethod("status", parameterClasses);
        String invoke = (String) method.invoke(openesbDaemon, (Object[]) parameters);    
        System.out.println(MessageFormat.format("At {1}, the status of the OpenESB instance is {0}.", invoke,LocalDateTime.now()));              
    }
    
    /**
     * Main method, used for testing only.
     *
     * @param args Command line arguments to be processed
     */
    public static void main(String args[]) {
        
        // Create CliArgs to parse the arguments
        CliArgs cliArgs = new CliArgs(args) ; 
        // Create three variables 
        String userName = null; 
        String password =  null;
        String target = "start" ; 
        userName = cliArgs.switchValue(mUserSwitch, "admin") ; 
        password = cliArgs.switchValue(mPasswordSwitch, "admin") ; 
        String [] targets = cliArgs.targets(); 
        int targetLength = targets.length;
        
        switch (targetLength) {
            case 0:
                target = "start";
                break;
            case 1:
                target = targets[0];
                break;
            default:
                mLog.log(Level.WARNING, "The start of OpenESB failed because of an incorrect command line");
                System.out.println("OpenESB starting failled because of a bad command line");
                System.out.println("Type: openesb");
                System.out.println("Type: openesb start or stop or status");
                System.out.println("*** with Security option ***");
                System.out.println("Type: openesb -u username -p password start or stop or status");                
                // Stop openesb Starting
                return;
        }

        if (daemon == null) {
            // Don't set daemon until init() has completed
            Bootstrap bootstrap = new Bootstrap();           
            daemon = bootstrap;
        }

        try {
            if (target.equals("start")) {
                daemon.start(userName, password);                
            } else if (target.equals("stop")) {
                daemon.stop (userName, password);
            } else if (target.equals("status")) {
                daemon.status (userName, password);
            } else {
                mLog.log(Level.WARNING,
                        "Bootstrap: command \"{0}\" does not exist.", target);
            }
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates a separate runtime classloader to avoid namespace pollution
     * between the component classloading hierarchy and the JBI implementation.
     * At present, this method is greedy and includes any file in the lib/
     * directory in the runtime classpath.
     */
    private void createFrameworkClassLoader() {
        ArrayList<URL> cpList = new ArrayList<URL>();
        URL[] cpURLs = new URL[0];
        File libDir = new File(
                System.getProperty(OPENESB_HOME_PROP), "lib");

        // Everything in the lib directory goes into the classpath
        for (File lib : libDir.listFiles()) {
            try {
                if (mBlacklistJars.contains(lib.getName())) {
                    // skip blacklisted jars
                    continue;
                }

                mLog.log(Level.FINEST, "Framework classloader : loading library {0}", lib.getName());
                cpList.add(lib.toURI().toURL());
            } catch (java.net.MalformedURLException urlEx) {
                mLog.log(Level.WARNING, "Bad library URL: {0}", urlEx.getMessage());
            }
        }

        cpURLs = cpList.toArray(cpURLs);
        mFrameworkClassLoader = new URLClassLoader(
                cpURLs, mExtensionClassLoader);
    }

    /**
     * Creates a separate extension classloader for the component classloading
     * chain. All jars added in the lib/ext directory are automatically added to
     * this classloader's classpath.
     */
    private void createExtensionClassLoader() {
        ArrayList<URL> cpList = new ArrayList<URL>();
        URL[] cpURLs = new URL[0];
        File libDir = new File(
                System.getProperty(OPENESB_HOME_PROP), "lib/ext");

        if (libDir.exists() || libDir.isDirectory()) {
            try {
                // Add the top-level ext directory
                cpList.add(libDir.toURI().toURL());

                // Everything in the lib/ext directory goes into the classpath
                for (File lib : libDir.listFiles()) {
                    mLog.log(Level.FINEST, "Extension classloader : loading library {0}", lib.getName());
                    cpList.add(lib.toURI().toURL());
                }
            } catch (java.net.MalformedURLException urlEx) {
                mLog.log(Level.WARNING, "Bad library URL: {0}", urlEx.getMessage());
            }
        }

        cpURLs = cpList.toArray(cpURLs);
        mExtensionClassLoader = new URLClassLoader(
                cpURLs, getClass().getClassLoader());
    }
}
