package net.openesb.standalone.startup;

import java.util.logging.LogManager;
import net.openesb.standalone.Lifecycle;
import net.openesb.standalone.node.Node;
import net.openesb.standalone.node.NodeBuilder;
import net.openesb.standalone.utils.ReflectionUtils;

/**
 * @author polperez @pymma 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Container implements Lifecycle {

    private final Node node;
    private Thread shutdownHook;

    public Container(boolean stopAction) {
        NodeBuilder nodeBuilder = NodeBuilder.nodeBuilder();
        if (! stopAction) {
            node = nodeBuilder.build();
        } else {
            node = nodeBuilder.buildStopNode();
        }
    }

    /**
     * Get status of The OpeneSB instance
     * @param credential
     * @param password
     * @return Started or Stopped
     */
    public String status(String credential, String password) {
        String status = node.status(credential, password);
        return status; 
    }
    
    
    /**
     * Stop the instance with a credential and a password
     * @param credential
     * @param password 
     */   
    public void stop (String credential, String password) {
         node.stop(credential, password);

        // This is a fix when we are trying to shutdown an instance by using 
        // a shutdown hook.
        try {
            ReflectionUtils.invoke(
                    LogManager.getLogManager(), "reset0");
        } catch (Throwable t) {
        }
    }
    
    /**
     * Start the instance with a credential an a password 
     * @param Credential
     * @param password
     */
    public void start(String credential, String password) {
        node.start(credential, password);
        // Register shutdown hook
        shutdownHook = new ContainerShutdownHook();
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }
    
    @Override
    public void start() {
        node.start();

        // Register shutdown hook
        shutdownHook = new ContainerShutdownHook();
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    /**
     * Stop an existing server instance.
     */
    @Override
    public void stop() {
        node.stop();

        // This is a fix when we are trying to shutdown an instance by using 
        // a shutdown hook.
        try {
            ReflectionUtils.invoke(
                    LogManager.getLogManager(), "reset0");
        } catch (Throwable t) {
        }
    }
    
    public void stopInstance() {
        node.stopInstance();

        // This is a fix when we are trying to shutdown an instance by using 
        // a shutdown hook.
        try {
            ReflectionUtils.invoke(
                    LogManager.getLogManager(), "reset0");
        } catch (Throwable t) {
        }
    }

    private class ContainerShutdownHook extends Thread {

        @Override
        public void run() {
            if (node != null) {
                Container.this.stopInstance();
            }
        }
    }
}
