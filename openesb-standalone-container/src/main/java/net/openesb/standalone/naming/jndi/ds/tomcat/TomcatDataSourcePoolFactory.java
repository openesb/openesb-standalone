package net.openesb.standalone.naming.jndi.ds.tomcat;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.sql.DataSource;
import javax.sql.XADataSource;
import javax.xml.bind.JAXBElement;
import net.openesb.standalone.LocalStringKeys;
import net.openesb.standalone.naming.jaxb.DataSourcePoolProperties;
import net.openesb.standalone.naming.jaxb.DataSourceProperties;
import net.openesb.standalone.naming.jaxb.PoolProperties;
import net.openesb.standalone.naming.jaxb.Property;
import net.openesb.standalone.naming.jndi.ds.DataSourcePoolFactory;
import net.openesb.standalone.utils.I18NBundle;

/**
 *
 * @author Paul PEREZ (paul.perez at pymma.com)
 * @author OpenESB Community
 */
public class TomcatDataSourcePoolFactory implements DataSourcePoolFactory {

    private static final Logger LOG = Logger.getLogger(TomcatDataSourcePoolFactory.class.getName());

    /* GetDatasource method is used to create dynamically and set up a pooled datasource. Information and parameters
     * are provided by dspProperties. The first part of the method create dynamically a native datasource. 
     * Introspection is used to set up datasource properties. We setup just the properties declared in 
     * context.xml (or else).
     * Using the same way, the second part setup Apache pool. Important: Pool Datasource property is 
     * set up with the native datasource, so there is no need for setting up other pool properties
     * related to the connection. 
     * Then we create an Apache datasource with the pool as parameter
     */
    @Override
    public DataSource getDataSource(DataSourcePoolProperties dspProperties) {
        try {
            org.apache.tomcat.jdbc.pool.PoolProperties poolProperties = this.createNativeDataSource(dspProperties);
            org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource(poolProperties);
            ds.setName(dspProperties.getDbConnectorName());
            registerMBean(ds);
            return ds;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_UNABLE_TO_CREATE_DATASOURCE, dspProperties.getDbConnectorName()), ex);

            return null;
        }
    }

    @Override
    public XADataSource getXADataSource(DataSourcePoolProperties dspProperties) {
        try {
            org.apache.tomcat.jdbc.pool.PoolProperties poolProperties = this.createNativeDataSource(dspProperties);
            org.apache.tomcat.jdbc.pool.XADataSource ds = new org.apache.tomcat.jdbc.pool.XADataSource(poolProperties);
            ds.setName(dspProperties.getDbConnectorName());
            registerMBean(ds);
            return ds;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_UNABLE_TO_CREATE_DATASOURCE, dspProperties.getDbConnectorName()), ex);
            return null;
        }
    }

    private void registerMBean(org.apache.tomcat.jdbc.pool.DataSource ds) throws Exception {
        try {
            ds.createPool();
        } catch (Exception ex) {
            System.out.println(ex);
        }
        try {
            ds.setJmxEnabled(true);
            MBeanServer mBeanServer = java.lang.management.ManagementFactory.getPlatformMBeanServer();
            String mBeanName = "net.open-esb.standalone:type=DataSources,name=" + ds.getName();
            mBeanServer.registerMBean(ds.getPool().getJmxPool(), new ObjectName(mBeanName));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_UNABLE_TO_CREATE_MBEAN, ds.getName()), ex);
        }
    }

    /**
     * 14/03/2018 Modification PPE
     *
     * Here we change the way to create the PoolProperties and use the
     * reflection at the method level first and the field level. Example: the
     * OpenESB user defines in the context.xml a file Url, the method search for
     * a setUrl method. If the method exists, we use it to setup the datasource.
     * if not we search for the Field Url and set it. If the field does not
     * exist
     *
     * @param dspProperties
     * @return PoolProperties
     * @throws Exception
     */
    private org.apache.tomcat.jdbc.pool.PoolProperties createNativeDataSource(DataSourcePoolProperties dspProperties) throws Exception {
        /* get the properties for the native Datasource. it is not created yet*/
        DataSourceProperties dataSourceProperties = dspProperties.getDataSourceProperties();
        Map<String, String> xmlDatasourceMap = this.listToMap(dataSourceProperties.getProperty());

        /* Get datasource name from OE Context. Native DS is created dynamically
         * so the class must be present in the classpath. DS Instance not created yet
         */
        String dsName = dspProperties.getDatasourceClassname();
        if (LOG.isLoggable(Level.INFO)) {
            LOG.log(Level.INFO, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_CREATE_DATASOURCE, dspProperties.getDbConnectorName()));
        }
        
        // Create a new Datastore class
        Class<?> dsClass;
        try {
            dsClass = Class.forName(dsName);
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_CLASS_NOT_FOUND, dsName, dspProperties.getDbConnectorName()));
            throw ex;
        }
        /**
         * Check if the class is a data sources
         * if not a exception is thrown
         */        
        /**
         * JIRA OCE 461 XA Datasource not supported
         * replace javax.sql.Datasource with javax.sql.commonDatasource
         */
        if (!(javax.sql.CommonDataSource.class.isAssignableFrom(dsClass))) {
            String dsMessage = String.format ("The class %s does not implement the interface javax.sql.DataSource", dspProperties.getDbConnectorName());             
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(dsMessage));                   
            String dsMessage01 = "OpenESB Standalone only supports Datasource drivers";
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(dsMessage01));                   
            throw (new ClassNotFoundException (dsMessage));
        }
                

        /* get the fields declared in a class and its ancesters */
        //Map<String, Field> dsFieldMap = this.getAllFields(dsClass);

        /* get the methods declared in a class and its ancesters */
        Map<String, Method> dsMethodMap = this.getAllMethods(dsClass);

        /**
         * Create datasource instance. This is the instance is updated thanks to
         * reflexion API and returned and to the caller The algorythm is: 1- get
         * the element name in the context.xml 2- set the first letter of the
         * element in capital 3- search the method set + element (ex: user
         * becomes setUser (element value) 4- if the method does not exists we
         * search for the field user with the element value
         */
        Object nativeDS;
        try {
            nativeDS = dsClass.newInstance();
        } catch (InstantiationException ex) {
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_UNABLE_TO_INSTANCIATE_CLASS, dsName, dspProperties.getDbConnectorName()));
            throw ex;
        } catch (IllegalAccessException ex) {
            LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_UNABLE_TO_ACCESS_CLASS, dsName, dspProperties.getDbConnectorName()));
            throw ex;
        }

        /**
         * Create pool configuration. The pool configuration contains a
         * datasource or a datadriver and also properties to setup the pool. We
         * setup the datasource or the datadriver first and Then the pool
         * properties
         */
        org.apache.tomcat.jdbc.pool.PoolProperties tomcatPoolProperties
                = new org.apache.tomcat.jdbc.pool.PoolProperties();

        /* get all the element in the config.xml*/
        Set<String> dspSet = xmlDatasourceMap.keySet();
        Iterator<String> keys = dspSet.iterator();

        /*Loop on the element from the XML context*/
        while (keys.hasNext()) {
            /* get elemt and value*/
            String xmlFieldName = keys.next();
            String xlmFieldValue = xmlDatasourceMap.get(xmlFieldName);

            /**
             * Capitalize the first letter of the name if needed and create the
             * method name ex: user becomes User and the method is set User
             */
            String fieldNameOriginal = xmlFieldName;
            xmlFieldName = xmlFieldName.substring(0, 1).toUpperCase() + xmlFieldName.substring(1);
            String methodName = "set" + xmlFieldName;

            /* Check if the method exists in the class */
            Method method = dsMethodMap.get(methodName);

            /* if the method is not null let set the parameters*/
            if (method != null) {
                /* get method parameters */
                if (method.getParameterCount() != 1) {
                    LOG.log(Level.WARNING, I18NBundle.getBundle().getMessage(
                            LocalStringKeys.DS_DATASOURCE_WRONG_NUMBER_OF_PARAMETERS_FOR_THE_METHOD, methodName, " Number of Prameter: ", method.getParameterCount()));
                } else {
                    /* prepare method invocation*/
                    boolean isAccessible = method.isAccessible();
                    method.setAccessible(true);

                    /* get the first parameter type of the method ( but at that level only one is avaialable) */
                    Class<?>[] parameterTypes = method.getParameterTypes();
                    Class<?> firstParameterClass = parameterTypes[0];

                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.log(Level.FINE, I18NBundle.getBundle().getMessage(
                                LocalStringKeys.DS_DATASOURCE_PROPERTY_SET, xmlFieldName, xlmFieldValue, dspProperties.getDbConnectorName()));
                    }
                    /**
                     * We evaluate the first parameter type and convert the
                     * value string from the context.xml and then invoke the
                     * method
                     */
                    try {
                        if (firstParameterClass.equals(byte.class)) {
                            method.invoke(nativeDS, Byte.parseByte(xlmFieldValue));
                        } else if (firstParameterClass.equals(boolean.class)) {
                            method.invoke(nativeDS, Boolean.parseBoolean(xlmFieldValue));
                        } else if (firstParameterClass.equals(char.class)) {
                            method.invoke(nativeDS, xlmFieldValue.charAt(0));
                        } else if (firstParameterClass.equals(short.class)) {
                            method.invoke(nativeDS, Short.parseShort(xlmFieldValue));
                        } else if (firstParameterClass.equals(int.class)) {
                            method.invoke(nativeDS, Integer.parseInt(xlmFieldValue));
                        } else if (firstParameterClass.equals(long.class)) {
                            method.invoke(nativeDS, Long.parseLong(xlmFieldValue));
                        } else if (firstParameterClass.equals(float.class)) {
                            method.invoke(nativeDS, Float.parseFloat(xlmFieldValue));
                        } else if (firstParameterClass.equals(double.class)) {
                            method.invoke(nativeDS, Double.parseDouble(xlmFieldValue));
                        } else if (firstParameterClass.equals(String.class)) {
                            method.invoke(nativeDS, xlmFieldValue);
                        } else if (firstParameterClass.equals(Object.class)) {
                            method.invoke(nativeDS, xlmFieldValue);
                        } else {
                            LOG.log(Level.WARNING, I18NBundle.getBundle().getMessage(
                                    LocalStringKeys.DS_DATASOURCE_PROPERTY_NOT_SET, xmlFieldName, "field.getType()", dspProperties.getDbConnectorName()));
                        }
                    } catch (IllegalArgumentException ex) {
                        LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                                LocalStringKeys.DS_DATASOURCE_PROPERTY_INVALID_VALUE, xlmFieldValue, xmlFieldName));
                        throw ex;
                    } catch (IllegalAccessException ex) {
                        LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                                LocalStringKeys.DS_DATASOURCE_PROPERTY_ACCESS, xmlFieldName));
                        throw ex;
                    } finally {
                        method.setAccessible(isAccessible);
                    }

                }

            } else {
                LOG.log(Level.WARNING,String.format("The method %s does not exist in the datasource %s. Please check the configuration of the config.xml",methodName,dsName));                
            }
        }


        /* Datasouce fields are set with data proterties found in the context 
             * Now let's set the pool with the pool proterties found in the context
             * get the properties for the pool 
         */
        if (LOG.isLoggable(Level.INFO)) {
            LOG.log(Level.INFO, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_DATASOURCE_PROPERTIES_SETTLED, dspProperties.getDbConnectorName()));
        }

        /**
         * ******************************************************************************************************************
         * After the DataSource we Setup Pool
         * *******************************************************************************************************************
         */
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, I18NBundle.getBundle().getMessage(
                    LocalStringKeys.DS_POOL_CONFIGURATION, dspProperties.getDbConnectorName()));
        }

        // Get values to configure the Pool from the XML context 
        PoolProperties contextPoolProperties = dspProperties.getPoolProperties();
        Map<String, String> xmlPoolMap = this.listToMap(contextPoolProperties.getProperty());

        Class poolPropertiesClass = tomcatPoolProperties.getClass();
        Map<String, Method> poolPropertiesMethod = this.getAllMethods(poolPropertiesClass);

        /* Use java reflexion to set up pool configuration with context properties*/
        Set<String> poolPropertiesSet = xmlPoolMap.keySet();
        keys = poolPropertiesSet.iterator();

        while (keys.hasNext()) {
            String xmlFieldName = keys.next();
            String xlmFieldValue = xmlPoolMap.get(xmlFieldName);
            /**
             * Capitalize the first letter of the name if needed and create the
             * method name ex: user becomes User and the method is set User
             */
            String fieldNameOriginal = xmlFieldName;
            xmlFieldName = xmlFieldName.substring(0, 1).toUpperCase() + xmlFieldName.substring(1);
            String methodName = "set" + xmlFieldName;
            /* Check if the method exists in the class */
            Method method = poolPropertiesMethod.get(methodName);

            /* if the method is not null set the parameters */
            if (method != null) {
                /* get method parameters */
                if (method.getParameterCount() != 1) {
                    LOG.log(Level.WARNING, I18NBundle.getBundle().getMessage(
                            LocalStringKeys.DS_DATASOURCE_WRONG_NUMBER_OF_PARAMETERS_FOR_THE_METHOD, methodName, " Number of Prameter: ", method.getParameterCount()));
                } else {
                    /* prepare method invocation*/
                    boolean isAccessible = method.isAccessible();
                    method.setAccessible(true);
                    /* get the first parameter type of the method ( but at that level only one is avaialable) */
                    Class<?>[] parameterTypes = method.getParameterTypes();
                    Class<?> firstParameterClass = parameterTypes[0];
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.log(Level.FINE, I18NBundle.getBundle().getMessage(
                                LocalStringKeys.DS_DATASOURCE_PROPERTY_SET, xmlFieldName, xlmFieldValue, dspProperties.getDbConnectorName()));
                    }
                    /**
                     * We evaluate the first parameter type and convert the
                     * value string from the context.xml and then invoke the
                     * method
                     */
                    try {
                        if (firstParameterClass.equals(byte.class)) {
                            method.invoke(tomcatPoolProperties, Byte.parseByte(xlmFieldValue));
                        } else if (firstParameterClass.equals(boolean.class)) {
                            method.invoke(tomcatPoolProperties, Boolean.parseBoolean(xlmFieldValue));
                        } else if (firstParameterClass.equals(char.class)) {
                            method.invoke(tomcatPoolProperties, xlmFieldValue.charAt(0));
                        } else if (firstParameterClass.equals(short.class)) {
                            method.invoke(tomcatPoolProperties, Short.parseShort(xlmFieldValue));
                        } else if (firstParameterClass.equals(int.class)) {
                            method.invoke(tomcatPoolProperties, Integer.parseInt(xlmFieldValue));
                        } else if (firstParameterClass.equals(long.class)) {
                            method.invoke(tomcatPoolProperties, Long.parseLong(xlmFieldValue));
                        } else if (firstParameterClass.equals(float.class)) {
                            method.invoke(tomcatPoolProperties, Float.parseFloat(xlmFieldValue));
                        } else if (firstParameterClass.equals(double.class)) {
                            method.invoke(tomcatPoolProperties, Double.parseDouble(xlmFieldValue));
                        } else if (firstParameterClass.equals(String.class)) {
                            method.invoke(tomcatPoolProperties, xlmFieldValue);
                        } else if (firstParameterClass.equals(Object.class)) {
                            method.invoke(tomcatPoolProperties, xlmFieldValue);
                        } else {
                            LOG.log(Level.WARNING, I18NBundle.getBundle().getMessage(
                                    LocalStringKeys.DS_DATASOURCE_PROPERTY_NOT_SET, xmlFieldName, "field.getType()", dspProperties.getDbConnectorName()));
                        }
                    } catch (IllegalArgumentException ex) {
                        LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                                LocalStringKeys.DS_DATASOURCE_PROPERTY_INVALID_VALUE, xlmFieldValue, xmlFieldName));
                        throw ex;
                    } catch (IllegalAccessException ex) {
                        LOG.log(Level.SEVERE, I18NBundle.getBundle().getMessage(
                                LocalStringKeys.DS_DATASOURCE_PROPERTY_ACCESS, xmlFieldName));
                        throw ex;
                    } finally {
                        method.setAccessible(isAccessible);
                    }
                }

            } else { 
                LOG.log(Level.WARNING,String.format("The method %s does not exist in the Tomcat PoolProperties. Please check the configuration of the config.xml",methodName));                
                
            }
        }

        // set the pool and get a Poolled Datasource  
        if (tomcatPoolProperties.getDataSource() == null) {
            LOG.log(Level.INFO, "Native Data Source Enabled");
            tomcatPoolProperties.setDataSource(nativeDS);
        }

        tomcatPoolProperties.setJmxEnabled(true);
        return tomcatPoolProperties;
    }

    /**
     * List to Map is an internal method used to convert a List<Property>
     * to a Map. Map will be use to set the DataSource and the Pool
     */
    private Map<String, String> listToMap(List<Property> inputList) {
        Map<String, String> outputMap = new HashMap<String, String>();
        Iterator<Property> it = inputList.iterator();
        while (it.hasNext()) {
            Property prop = it.next();
            List<JAXBElement<String>> nameAndValueAndDescription = prop.getNameAndValueAndDescription();
            Iterator<JAXBElement<String>> it2 = nameAndValueAndDescription.iterator();
            String key = null, value = null;
            while (it2.hasNext()) {
                JAXBElement<String> element = it2.next();
                String localpart = element.getName().getLocalPart();
                if ("name".equals(localpart)) {
                    key = element.getValue();
                } else if ("value".equals(localpart)) {
                    value = element.getValue();
                }
                // Put the key valu in the Map
                outputMap.put(key, value);
            }
        }
        return outputMap;
    }

    /* getAllField is used to get all the fields declared in a class + its ancesters
     * getDeclaredField just returns the field declared at the class level and not 
     * at the ancester levels. The retured map contains key-values pairs with Field name and Field object
     */
    private Map<String, Field> getAllFields(Class<?> type) {
        List<Field> listFields = new ArrayList<Field>();
        for (Class<?> c = type; c != Object.class; c = c.getSuperclass()) {
            listFields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        Map<String, Field> mapFields = new HashMap<String, Field>();
        Iterator<Field> fieldIterator = listFields.iterator();

        while (fieldIterator.hasNext()) {
            Field field = fieldIterator.next();
            String fieldName = field.getName();
            mapFields.put(fieldName, field);
        }
        return mapFields;
    }

    /* getAllMethod is used to get all the method declared in a class + its ancesters
     * getDeclaredMethod just returns the methods declared at the class level and not 
     * at the ancester levels. The retured List returns the list of the method 
     * available for the class and their paramters. 
     */
    private Map<String, Method> getAllMethods(Class<?> type) {
        List<Method> listMethods = new ArrayList<>();
        for (Class<?> c = type; c != Object.class; c = c.getSuperclass()) {
            listMethods.addAll(Arrays.asList(c.getDeclaredMethods()));
        }
        Map<String, Method> mapMethods = new HashMap<>();
        Iterator<Method> methodIterator = listMethods.iterator();

        while (methodIterator.hasNext()) {
            Method method = methodIterator.next();
            String methodName = method.getName();            
            /**
             * Get only public method 
             */
            if (Modifier.isPublic(method.getModifiers())){
                mapMethods.put(methodName, method);
            }   
        }
        return mapMethods;
    }
}
