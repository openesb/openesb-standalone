package net.openesb.standalone.node;

/**
 * @author polperez
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 * 
 * poperez uses default word to add credential and password to the instance node. 
 * There is maybe a cleaner way to do it. But I did not find one for the moment 
 * Sorry
 * 
 */
public interface Node {

    /**
     * Start the node. If the node is already started, this method is no-op.
     */
    default void start() {
        
    }

    /**
     * Stops the node. If the node is already stopped, this method is no-op.
     */
    default void stop() {
        
    }
        
    void stopInstance();
    
    /**
     * Returns the node name.
     * @return  The node name.
     */
    String name();
    
    /**
     * Update the interface polperez 
     */
        
    /**
     * Get instance status 
     * @param credential
     * @param password
     * @return Started if the instance is started Stopped
     */
    default String status (String credential, String password) {
        return null;
    } 
    
    /**
     * Stop the node with a credential 
     * @param credential
     * @param password
     */
    default void stop (String credential, String password){
      //no-op  
    }
    
    default void start (String credential, String password){
        // no-op
    }
}
