## ****************************************************************************
## The contents of this file are subject to the terms
## of the Common Development and Distribution License
## (the "License").  You may not use this file except
## in compliance with the License.
## You can obtain a copy of the license at
## www.opensource.org/licenses/CDDL-1.0.
## See the License for the specific language governing
## permissions and limitations under the License.
##

##  Copyright OpenESB Community 2015-2022.
## ****************************************************************************

## ****************************************************************************
## Generate an encrypted password from a plain text password for OpenESB access.
## The encrypted password is not a base 64 but is specific to OpenESB
##
## ex: create_password mypassword returns DLU8cWEY4cHdtH4ohLQlcw==  
##
## ****************************************************************************

if (( $# == 0 )); then
    echo
    echo "The plain password must be provided as parameter"
    echo "ex: create_password mypassword returns DLU8cWEY4cHdtH4ohLQlcw== "
    echo
else
    echo
	java -cp "../lib/*" net.openesb.standalone.security.utils.PasswordManagement $1
    echo
fi

