:: ***************************************************************
:: The contents of this file are subject to the terms
:: of the Common Development and Distribution License
:: (the "License").  You may not use this file except
:: in compliance with the License.
:: You can obtain a copy of the license at
:: www.opensource.org/licenses/CDDL-1.0.
:: See the License for the specific language governing
:: permissions and limitations under the License.
::
:: When distributing Covered Code, include this CDDL
:: HEADER in each file and include the License file at
:: https://open-esb.dev.java.net/public/CDDLv1.0.html. 
:: If applicable add the following below this CDDL HEADER,
:: with the fields enclosed by brackets "[]" replaced with
:: your own identifying information: Portions Copyright
:: [year] [name of copyright owner]
::
::
::  Copyright OpenESB Community 2022.
:: *****************************************************************

:: ====================================================================== 
::                                                                         
::  Generate an encrypted password from a plain password
::  ex: create_password mypassword returns DLU8cWEY4cHdtH4ohLQlcw==                         
::                                                                         
:: ====================================================================== 


::****************** set configuration variables ***********************
:: Check if Java home is set
::****************** set configuration variables ***********************
@echo off
IF "%JAVA_HOME%"=="" GOTO nojavahome
IF "%1"=="" GOTO nopassword

%JAVA_HOME%\bin\java -cp "../lib/*" net.openesb.standalone.security.utils.PasswordManagement %1
GOTO endbatch

:nojavahome
echo.
echo =========================================================================
echo.
echo. WARNING ...
echo. Please set JAVA_HOME before starting create_password 
echo. Please check Java documentation to do it 
echo.
echo =========================================================================
GOTO endbatch

:nopassword
echo. 
echo =========================================================================
echo WARNING ...
echo The plain password must be passed as parameter 
echo Please check OpenESB documentation 
echo =========================================================================
echo.
GOTO endbatch

:endbatch